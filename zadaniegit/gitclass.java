

package zadaniegit;
import javax.swing.*; 
import java.awt.*; 
import java.awt.event.*;

public class gitclass 
{
  
  public static void main(String args[])
  {
    Okno okno=new Okno(); 
    okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    okno.setVisible(true); 
  }
}  

class Okno extends JFrame 
{
  public Okno() 
  {
    setTitle("Kalkulator"); 
    setSize(160,250); 
    
    
    MojPanel p=new MojPanel(); 
    Container powzaw=getContentPane(); 
    powzaw.add(p); 
  }
}
  
class MojPanel extends JPanel 
{
  
  JTextField a;     
  JTextField b;     
  JTextField wynik; 
  
  
  
  public MojPanel() 
  {
    setLayout(new FlowLayout(FlowLayout.LEFT));  
                                                
                                                

  
    JLabel lab1=new JLabel("Liczba Pierwsza");    
    add(lab1);                             
    
    a=new JTextField("",12);               
    add(a);                               
    
    JLabel lab2=new JLabel("Liczba Druga");    
    add(lab2);                             
    
    b=new JTextField("",12);              
    add(b);                               
        
    
    JButton plus=new JButton("Dodaj");        
    add(plus);                            
    
    JButton minus=new JButton("Odejmij");       
    add(minus);                           
    
    JButton mnozenie=new JButton("Pomnoz");    
    add(mnozenie);                        
    
    JButton dzielenie=new JButton("Podziel");   
    add(dzielenie);        
    JButton srd=new JButton("Średnia");        
    add(srd);
    JButton min=new JButton("MinMaX");        
    add(min);
    
    
    
    ActionListener sl1=new Dodaj();       
    plus.addActionListener(sl1);         
    
    ActionListener sl2=new Odejmij();     
    minus.addActionListener(sl2);          
    
    ActionListener sl3=new Pomnoz();      
    mnozenie.addActionListener(sl3);        
    
    ActionListener sl4=new Podziel();     
    dzielenie.addActionListener(sl4);      
    ActionListener sl5=new Min();       
    plus.addActionListener(sl5);   
    ActionListener sl6=new Srednia();       
    plus.addActionListener(sl6);   
    
    JLabel lab3=new JLabel("wynik");      
    add(lab3);                            
    
    wynik=new JTextField("",12);          
    add(wynik);                           
    
    }
  
  class Dodaj implements ActionListener   
  {
    public void actionPerformed(ActionEvent zdarzenie) 
    {
      long n=Integer.parseInt(a.getText()); 
      long m=Integer.parseInt(b.getText()); 
      long suma=n+m; 
      wynik.setText(""+suma+" "); 
    }
  }
  
    class Odejmij implements ActionListener
  {
    public void actionPerformed(ActionEvent zdarzenie) 
    {
      long n=Integer.parseInt(a.getText()); 
      long m=Integer.parseInt(b.getText()); 
      long roznica=n-m; 
      wynik.setText(""+roznica); 
    }
  }
    
  class Pomnoz implements ActionListener
  {
    public void actionPerformed(ActionEvent zdarzenie) 
    {
      long n=Integer.parseInt(a.getText()); 
      long m=Integer.parseInt(b.getText()); 
      long iloczyn=n*m; 
      wynik.setText(""+iloczyn); 
    }
  }
      
  class Podziel implements ActionListener
  {
    public void actionPerformed(ActionEvent zdarzenie) 
    {
      long n=Integer.parseInt(a.getText()); 
      long m=Integer.parseInt(b.getText()); 
      float iloraz=(float)n/m; 
                               
      wynik.setText(""+iloraz); 
    }
  }
  
  class Min implements ActionListener   
  {
    public void actionPerformed(ActionEvent zdarzenie) 
    {
      long n=Integer.parseInt(a.getText()); 
      long m=Integer.parseInt(b.getText()); 
      boolean min=n==m; 
      wynik.setText("min "); 
    }
  }
  class Srednia implements ActionListener
  {
    public void actionPerformed(ActionEvent zdarzenie) 
    {
      long n=Integer.parseInt(a.getText()); 
      long m=Integer.parseInt(b.getText()); 
      long srd=n*m/2; 
      wynik.setText(""+srd); 
    }
  }
      
  
  
  
	}

